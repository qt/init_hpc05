#!/usr/bin/env bash

export CONDA_DIR="${HOME}/miniconda3"

# Create a temporary folder
mkdir -p ~/tmp && cd ~/tmp

# Remove the old miniconda
rm -fr $CONDA_DIR && mkdir -p $CONDA_DIR

# Download miniconda
wget --quiet https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh

# Install Miniconda and add channels
/bin/bash Miniconda3-latest-Linux-x86_64.sh -f -b -p $CONDA_DIR
$CONDA_DIR/bin/conda config --system --add channels conda-forge
$CONDA_DIR/bin/conda config --system --set auto_update_conda false

# Clone quantum-tinkerer/research-docker to get the latest envs.yml
git clone https://gitlab.kwant-project.org/qt/research-docker.git

# Update the root environment
$CONDA_DIR/bin/conda env update -n base -f research-docker/main_image/python3.yml

# Cleanup
$CONDA_DIR/bin/conda clean -tipsy
rm -fr ~/tmp
