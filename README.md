# Initialize files and conda environments on the hpc05

* ssh into the `hpc05`

* Download the file repository with
```bash
cd ~/; git clone https://gitlab.kwant-project.org/qt/init_hpc05.git
```

* Copy the `.bash_profile` and `.bashrc` with
```bash
bash ~/init_hpc05/init_profile.sh
```

* Install `conda` and the correct environments with
```bash
bash ~/init_hpc05/install_conda.sh
```

*By default it will use the `dev` environment which is the kwant master, to change this edit the `.bash_profile`, using `nano ~/.bash_profile`*

## Updating the conda environment to match `io`, simply the following command again:
```bash
bash ~/init_hpc05/install_conda.sh
```

## Installing a kwant branch on the `hpc05`
```bash
# bash ~/init_hpc05/install_kwant_branch.sh BRANCH_NAME ENV_NAME, for example:
bash ~/init_hpc05/install_kwant_branch.sh master kwant_master
```
