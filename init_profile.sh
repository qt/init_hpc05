cp ~/init_hpc05/bash_profile ~/.bash_profile
cp ~/init_hpc05/bashrc ~/.bashrc
cp ~/init_hpc05/stat.py ~/stat.py
wget -O ~/stat.py https://gitlab.kwant-project.org/qt/cookbook/raw/master/hpc05-usage.py
echo "first: run 'source ~/.bash_profile' to immediately use the new bash_profile"
echo "now run 'bash ~/init_hpc05/install_conda.sh' to install conda"
