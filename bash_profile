#!/bin/bash

export PATH="${HOME}/miniconda3/bin:$PATH"
# source activate dev

alias del="qselect -u $USER | xargs qdel; rm -f *.hpc05.hpc* ipengine* ipcontroller* pbs_*; pkill -f hpc05_culler 2> /dev/null; pkill -f ipcluster 2> /dev/null; pkill -f ipengine 2> /dev/null; pkill -f ipyparallel.controller 2> /dev/null; pkill -f ipyparallel.engines 2> /dev/null"
alias stat="python ~/stat.py"
