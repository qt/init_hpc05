#!/usr/bin/env bash

export BRANCH=$1
export ENV_NAME=$2

if [ -z "$BRANCH" ]; then
    export BRANCH='master'
fi

if [ -z "$ENV_NAME" ]; then
    export ENV_NAME='kwant'
fi

export CONDA_DIR="${HOME}/miniconda3"

# Create a temporary folder
mkdir -p ~/tmp && cd ~/tmp

# Clone quantum-tinkerer/research-docker to get the latest envs.yml
git clone https://github.com/quantum-tinkerer/research-docker.git

# Remove the old environment if it exists
conda env remove --yes --name $ENV_NAME

# Add a new environment (same as dev on io)
CONDA_ALWAYS_COPY=true $CONDA_DIR/bin/conda env create -p $CONDA_DIR/envs/$ENV_NAME -f research-docker/dev.yml

# Download and install kwant
git clone https://gitlab.kwant-project.org/kwant/kwant.git && cd kwant
git checkout $BRANCH  # get the branch you want to install
source activate $ENV_NAME
conda remove --yes kwant tinyarray
conda install --yes gcc lapack mumps cython pytest pytest-cov
pip install git+https://gitlab.kwant-project.org/kwant/tinyarray.git

# Create build.conf file
cat > build.conf << EOF
[mumps]
include_dirs = $CONDA_DIR/envs/$ENV_NAME/include
library_dirs = $CONDA_DIR/envs/$ENV_NAME/lib
libraries = zmumps mumps_common pord metis esmumps scotch scotcherr mpiseq gfortran openblas
extra_link_args = -Wl,-rpath=$CONDA_DIR/envs/$ENV_NAME/lib
EOF

# Install kwant
pip install .

# Cleanup
$CONDA_DIR/bin/conda clean -tipsy
rm -fr ~/tmp
